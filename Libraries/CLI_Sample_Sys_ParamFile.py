# =====================================================================================
#    Filename   :  CLI_Sample_Sys_ParamFile.py
#
#    Description:  This param file contains all the user defined values used for
#               :  this frame work.
#
#    Version    :  1.0
#    Created    :  14 Dec 2017
#    Compiler   :  python
#    Author     :  Ashin Basil John
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
# =====================================================================================

###################################################################
# KeyWord Name : listofnames
# Usage        : 
# command	   : 
#
###################################################################

listofnames=['systesting']

###################################################################
# KeyWord Name : systesting
# Usage        : List containing functions defined to issue commands and check response
# command	   : if the function Shell_Command receives value dantel then  it will issued the command pml.
#
###################################################################

systesting=[
	"Shell_Command('pml','dantel')",
	"Shell_Command('sys','Pointmaster> ')",
	"Kid_Expect('MAT    1 . . . . . . . . . . . . . . . .    1- 16')",
	"Kid_Expect('MAT    2 . . . . . . . . . . . . . . . .   17- 32')",
	"Kid_Expect('MAT    3 . . . . . . . . . . . . . . . .   33- 48')",
	"Kid_Expect('MAT    4 . . . . . . . . . . . . . . . .   49- 64')",
	"Kid_Expect('MAT    5 . . . . . . . . . . . . . . . .   65- 80')",
	"Kid_Expect('MAT    6 . . . . . . . . . . . . . . . .   81- 96')",
	"Kid_Expect('MAT    7 . . . . . . . . . . . . . . . .   97-112')",
	"Kid_Expect('MAT    8 . . . . . . . . . . . . . . . .  113-128')",
	"Kid_Expect('MAT    9 . . . . . . . . . . . . . . . .  129-144')",
	"Kid_Expect('MAT   10 . . . . . . . . . . . . . . . .  145-160')",
	"Kid_Expect('MAT   11 . . . . . . . . . . . . . . . .  161-176')",

	"Shell_Command('exit','Pointmaster> ')",
	"Shell_Command('exit','dantel')",
	]